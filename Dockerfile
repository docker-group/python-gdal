FROM python:3.6.8

ENV DEBIAN_FRONTEND="teletype" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8"

RUN pip install pip --upgrade && \
    apt-get update && \
    apt-get install -y git gdal-bin nano less wait-for-it gettext

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
